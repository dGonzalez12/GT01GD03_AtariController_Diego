using UnityEngine;
using UnityEngine.Events;

public class GameManager:MonoBehaviour{
	public static GameManager Instance;

	[HideInInspector]
	public UnityEvent OnPlayerWon;
	[HideInInspector]
	public UnityEvent OnPlayerLost;

	private void Awake(){
		Instance = this;
	}

	private void Start(){
		ScoreManager.Initialize();
	}


	private void CompletePlayerWin(){
		//Take a few seconds
		//Show end of screen menu		
		PlayerWin();
	}

	private void PlayerLost(){
		OnPlayerLost?.Invoke();
	}

	private void PlayerWin(){
		OnPlayerWon?.Invoke();
	}
}