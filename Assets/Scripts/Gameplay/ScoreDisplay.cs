using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class ScoreDisplay : MonoBehaviour{

	[SerializeField]
	private TMP_Text _displayText = null;

	[SerializeField]
	private float _animTime = 1f;

	private void OnEnable(){
		ScoreManager.OnValueChanged += UpdateScore;
	}

	private void OnDisable()
	{
		ScoreManager.OnValueChanged -= UpdateScore;
	}

	void UpdateScore(int prevValue, int current){
		StopCoroutine(ScoreUpdateRoutine(prevValue, current));
		SetScoreText(prevValue);
		StartCoroutine(ScoreUpdateRoutine(prevValue , current));
	}

	private void SetScoreText(int valu){
		if(_displayText){
			_displayText.text = $"Score: {valu}";
		}
	}

	IEnumerator ScoreUpdateRoutine(int prev, int current){
		yield return null;
		for(float t = 0; t <= _animTime; t+= Time.fixedDeltaTime){
			yield return new WaitForFixedUpdate();
			SetScoreText(
				Mathf.FloorToInt(
					Mathf.Lerp(
						prev,
						current,
						t
					)
				)
			);
		}
		SetScoreText(current);
	}
}