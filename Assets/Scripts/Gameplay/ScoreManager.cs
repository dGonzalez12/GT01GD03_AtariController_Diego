using System;
using UnityEngine;

public class ScoreManager: MonoBehaviour{

	public static event Action<int, int> OnValueChanged;

	private static int _currentScore;

	public static int CurrentScore{
		get => _currentScore;
		set {
			int prevValue = _currentScore;
			_currentScore = value;
			OnValueChanged?.Invoke(prevValue, value);
		}
	}

	public static void Initialize(){
		CurrentScore = 0;
	}
}
