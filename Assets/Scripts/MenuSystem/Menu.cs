﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum LoadSubmenusOption{hierarchy,findType,manual};

public class Menu : MonoBehaviour {

	[SerializeField]
	protected List<Submenu> _containedMenus;
	[SerializeField] LoadSubmenusOption _loadOption = LoadSubmenusOption.hierarchy;

	public Button _defaultButton;

	[Header("Scene Loading")]
	[SerializeField] Image _loadBar = null;

	//Mobile only
	
	[Header("Screen Options")]
	[SerializeField] 
	private bool _overrideScreenOrientation = false;
	[SerializeField] 
	private ScreenOrientation _onStartOrientation = ScreenOrientation.Landscape;

	public void InitializeMenu(Submenu menu){
		menu.GetComponent<RectTransform> ().anchoredPosition = new Vector2(0,0);
		menu.Hide();
	}

	public void HideAll(){
		foreach(Submenu sub in _containedMenus){
			sub.Hide();
		}
	}

	public void GoToScene(string levelName){
		//normalize time scale
		Time.timeScale = 1.0f;
		DisableInteraction();
		SceneManager.LoadScene (levelName);
	}

	public void RestartThisLevel(){
		GoToScene(SceneManager.GetActiveScene().name);
	}

	public void LoadScenePretty(string sceneName){
		DisableInteraction();
		StartCoroutine(LoadSceneWithScreen(sceneName));
	}

	IEnumerator LoadSceneWithScreen(string levelName){
		
		Time.timeScale = 1.0f;
		AsyncOperation async = SceneManager.LoadSceneAsync(levelName);
		while(!async.isDone){
			if(_loadBar){
				_loadBar.fillAmount = Mathf.Clamp01(async.progress / 0.9f);
			}
			yield return null;
		}
	}

	public virtual void Start () {
		if(_loadOption == LoadSubmenusOption.hierarchy){
			_containedMenus = new List<Submenu>();
			for(int i = 0; i < transform.childCount; i++){
				Submenu newSubmenu = transform.GetChild(i).GetComponent<Submenu>(); 
				if(newSubmenu != null){
					_containedMenus.Add(newSubmenu);
				}
			}
		}else if(_loadOption == LoadSubmenusOption.findType){
			Submenu[] tempArraySubmenu = GetComponentsInChildren<Submenu>();
			_containedMenus = new List<Submenu>();
			for(int i = 0; i < tempArraySubmenu.Length; i++){
				_containedMenus.Add(tempArraySubmenu[i]);
			}
		}
		
		OrganizeMenus ();

		if(_overrideScreenOrientation){
			Screen.orientation = _onStartOrientation;
		}
	}

	public void OrganizeMenus (){
		for (int i = 0; i < _containedMenus.Count; i++) {
			InitializeMenu (_containedMenus [i]);
		}
		_containedMenus[0].Show();
	}

	public void NavigateTo(string url){
		Application.OpenURL(url);
	}

	public void SelectButton(Button selectableButton){
		if(selectableButton.interactable){
			selectableButton.Select();
		}
	}

	public void QuitGame () {
		Application.Quit();
	}

	public void DebugEvent(string eventName){
		// Debug.Log("Event " + eventName + " Called at " + Time.time);
	}

	public void DisableInteraction(){
		foreach (Submenu item in _containedMenus){
			item.GetComponent<CanvasGroup>().interactable = false;
		}
	}

	public void SetScreenOrientation(ScreenOrientation newOrientation){
		Screen.orientation = newOrientation;
	}
}
