﻿using UnityEngine;

public static class ExtensionMethods{

    public static Vector3 WithHeightOf(this Vector3 v, float newHeight){
        return new Vector3(
            v.x,
            newHeight,
            v.z
        );
    }

}
