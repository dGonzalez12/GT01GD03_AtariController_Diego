﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class LifeSpan : MonoBehaviour{
    [Tooltip("-1 means infinite lifetime")]
    [SerializeField]
    private float _lifespan = -1;

    public UnityEvent onSpawn;

    private void Awake(){
        if(_lifespan >= 0){
            Destroy(gameObject, _lifespan);
        }
    }

    private void OnEnable()
    {
        onSpawn.Invoke();
    }

}

